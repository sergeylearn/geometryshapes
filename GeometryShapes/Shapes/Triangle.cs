﻿using System;

namespace GeometryShapes.Shapes
{
    public class Triangle
    {
        private double Side1 { get; set; }
        private double Side2 { get; set; }
        private double Side3 { get; set; }

        public Triangle(double side1, double side2, double side3)
        {
            Side1 = side1;
            Side2 = side2;
            Side3 = side3;
        }

        public double GetArea()
        {
            var semiperimeter = GetPerimeter() / 2;
            Console.WriteLine(semiperimeter);
            return Math.Sqrt(semiperimeter * (semiperimeter-Side1) * (semiperimeter - Side2) * (semiperimeter - Side3));
        }

        public double GetPerimeter()
        {
            return Side1 + Side2 + Side3;
        }
    }
}
